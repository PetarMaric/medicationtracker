package hr.ferit.petarmaric.medicationtracker.ui.drugadd

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import hr.ferit.petarmaric.medicationtracker.R
import hr.ferit.petarmaric.medicationtracker.common.displayToast
import hr.ferit.petarmaric.medicationtracker.model.Drug
import hr.ferit.petarmaric.medicationtracker.notificationscheduler.AlarmReceiver
import hr.ferit.petarmaric.medicationtracker.notificationscheduler.NotificationScheduler
import hr.ferit.petarmaric.medicationtracker.persistence.DrugsRepository
import hr.ferit.petarmaric.medicationtracker.persistence.DrugsRoomRepository
import kotlinx.android.synthetic.main.fragment_add_drug.*
import java.time.LocalDate
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneOffset

class DrugAddFragment : Fragment(), TimePickerFragment.TimeSetListener {

    private var hour: Int = 0
    private var minute: Int = 0

    private val repository: DrugsRepository = DrugsRoomRepository()

    private var timeDateToSave: OffsetDateTime = OffsetDateTime.of(0, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC)

    private lateinit var timeToSave: LocalTime


    override fun timePicked(hourParam: Int, minuteParam: Int) {
        hour = hourParam
        minute = minuteParam


        timeSelectedText.text = "$hour:$minute"

        timeDateToSave = OffsetDateTime.of(LocalDate.now(), LocalTime.of(hourParam, minuteParam), ZoneOffset.UTC)
        timeToSave = LocalTime.of(hourParam, minuteParam)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_add_drug, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setUpUi()
    }

    private fun setUpUi() {

        val timePickerDialog = TimePickerFragment()
        timePickerDialog.setTimePickedListener(this)

        showTimePickerDialog.setOnClickListener { timePickerDialog.show(childFragmentManager, "time_picker") }

        saveDrugButton.setOnClickListener { saveDrug() }


    }

    private fun saveDrug() {

        if (isInputInvalid()) {
            context?.displayToast(getString(R.string.checkFormText))
            return
        }

        val newDrug = Drug(
            title = drugName.text.toString(),
            drugCount = drugCount.text.toString().toInt(),
            alarmTime = timeDateToSave.toLocalTime()
        )

        val insertedDrugId = repository.addDrug(newDrug)

        val drug = repository.getDrug(insertedDrugId.toInt())

        NotificationScheduler.setReminder(context!!, AlarmReceiver::class.java, drug)

        clearInputFields()
        context?.displayToast(getString(R.string.addDrugSuccessText))
    }

    private fun clearInputFields() {
        drugName.text.clear()
        drugCount.text.clear()
        timeDateToSave = timeDateToSave.withYear(0)
        timeSelectedText.text = ""
    }

    private fun isInputInvalid(): Boolean {
        if (drugName.text.isNullOrEmpty() || drugCount.text.isNullOrEmpty() || timeDateToSave.year == 0) {
            return true
        }
        return false
    }

}
