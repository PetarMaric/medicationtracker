package hr.ferit.petarmaric.medicationtracker.persistence

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import hr.ferit.petarmaric.medicationtracker.RootAppActivity
import hr.ferit.petarmaric.medicationtracker.model.Drug


@Database(entities = [Drug::class], version = 1, exportSchema = false)
@TypeConverters(RoomTypeConverters::class)
abstract class DrugsDatabase : RoomDatabase() {

    abstract fun getDrugsDao(): DrugsDao

    companion object {
        const val DB_NAME = "DrugsDatabase"

        private val dbInstance by lazy {
            Room.databaseBuilder(
                RootAppActivity.getAppContext(),
                DrugsDatabase::class.java,
                DB_NAME
            ).allowMainThreadQueries()
                .build()
        }

        val drugsDao by lazy { dbInstance.getDrugsDao() }
    }


}