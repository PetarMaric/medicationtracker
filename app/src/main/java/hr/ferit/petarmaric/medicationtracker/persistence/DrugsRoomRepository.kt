package hr.ferit.petarmaric.medicationtracker.persistence

import hr.ferit.petarmaric.medicationtracker.model.Drug

class DrugsRoomRepository : DrugsRepository {

    private val drugsDao = DrugsDatabase.drugsDao


    override fun deleteDrug(drug: Drug) {
        drugsDao.deleteDrugFromDatabase(drug)
    }

    override fun getAllDrugs(): MutableList<Drug> {
        return drugsDao.getAllDrugs()
    }

    override fun addDrug(drug: Drug): Long {
        return drugsDao.addDrugToDatabase(drug)
    }

    override fun getDrug(drugId: Int): Drug {
        return drugsDao.getDrug(drugId)
    }

    override fun updateDrug(drug: Drug) {
        drugsDao.updateDrug(drug)
    }

    override fun deleteAllDrugs() {
        drugsDao.deleteAllDrugs()
    }
}