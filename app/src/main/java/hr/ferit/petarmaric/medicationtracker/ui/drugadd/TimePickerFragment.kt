package hr.ferit.petarmaric.medicationtracker.ui.drugadd

import android.app.Dialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.util.Log
import android.widget.TimePicker
import androidx.fragment.app.DialogFragment
import java.util.*

class TimePickerFragment : DialogFragment(), TimePickerDialog.OnTimeSetListener {


    interface TimeSetListener {
        fun timePicked(hourParam: Int, minuteParam: Int)
    }


    private var timePickedListener: TimeSetListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val calendar = Calendar.getInstance()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minute = calendar.get(Calendar.MINUTE)

        return TimePickerDialog(activity, this, hour, minute, true)
    }

    override fun onTimeSet(view: TimePicker?, hourOfDay: Int, minute: Int) {
        Log.d("SELECTED_TIME", "Hour: ${hourOfDay}, minute: ${minute}\n")

        timePickedListener?.timePicked(hourOfDay, minute)
    }

    fun setTimePickedListener(listener: TimeSetListener) {
        timePickedListener = listener
    }
}