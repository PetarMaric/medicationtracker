package hr.ferit.petarmaric.medicationtracker.persistence

import androidx.room.*
import hr.ferit.petarmaric.medicationtracker.model.Drug

@Dao
interface DrugsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addDrugToDatabase(drug: Drug): Long

    @Delete
    fun deleteDrugFromDatabase(drug: Drug)

    @Query("SELECT * FROM Drugs")
    fun getAllDrugs(): MutableList<Drug>

    @Query("SELECT * FROM Drugs WHERE id=:drugId")
    fun getDrug(drugId: Int): Drug

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateDrug(drug: Drug)

    @Query("DELETE from drugs")
    fun deleteAllDrugs()

}