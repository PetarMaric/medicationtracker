package hr.ferit.petarmaric.medicationtracker.ui.listeners

import android.view.MenuItem
import androidx.fragment.app.FragmentActivity
import com.google.android.material.bottomnavigation.BottomNavigationView
import hr.ferit.petarmaric.medicationtracker.R
import hr.ferit.petarmaric.medicationtracker.common.showFragment
import hr.ferit.petarmaric.medicationtracker.ui.drugadd.DrugAddFragment
import hr.ferit.petarmaric.medicationtracker.ui.druglist.DrugListFragment

class BottomNavigationListener(private val context: FragmentActivity) :
    BottomNavigationView.OnNavigationItemSelectedListener {
    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.drugList -> {
                context.showFragment(
                    R.id.fragmentContainer,
                    DrugListFragment()
                )
            }

            R.id.drugAdd -> {
                context.showFragment(R.id.fragmentContainer, DrugAddFragment())
            }
        }
        return true
    }

}