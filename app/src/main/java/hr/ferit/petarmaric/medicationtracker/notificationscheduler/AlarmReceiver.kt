package hr.ferit.petarmaric.medicationtracker.notificationscheduler

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import hr.ferit.petarmaric.medicationtracker.MainActivity
import hr.ferit.petarmaric.medicationtracker.persistence.DrugsRepository
import hr.ferit.petarmaric.medicationtracker.persistence.DrugsRoomRepository

// https://developer.android.com/training/scheduling/alarms
// https://droidmentor.com/schedule-notifications-using-alarmmanager/

class AlarmReceiver : BroadcastReceiver() {

    private val repository: DrugsRepository = DrugsRoomRepository()

    companion object {
        const val TAG = "AlarmReceiver"
        const val DRUG_ID = "DrugId"
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action != null && context != null) {
            if (intent.action.equals(Intent.ACTION_BOOT_COMPLETED, true)) {
                Log.d(TAG, "broadcast boot completed")

                val drugList = repository.getAllDrugs()
                drugList.forEach { NotificationScheduler.setReminder(context, AlarmReceiver::class.java, it) }

                return
            }


        }
        val drugId = intent?.getIntExtra(DRUG_ID, 0) ?: 0

        val drug = repository.getDrug(drugId)

        NotificationScheduler.showNotification(
            context!!, MainActivity::class.java, drug
        )


    }
}