package hr.ferit.petarmaric.medicationtracker.persistence

import hr.ferit.petarmaric.medicationtracker.model.Drug

interface DrugsRepository {

    fun addDrug(drug: Drug): Long

    fun deleteDrug(drug: Drug)

    fun getAllDrugs(): MutableList<Drug>

    fun getDrug(drugId: Int): Drug

    fun updateDrug(drug: Drug)

    fun deleteAllDrugs()
}