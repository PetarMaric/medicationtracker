package hr.ferit.petarmaric.medicationtracker.ui.druglist

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.petarmaric.medicationtracker.model.Drug
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_drug.*

class DrugViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView), LayoutContainer {
    fun bind(
        drug: Drug,
        onDrugClickedListener: (Drug) -> Unit,
        onTitleSelectedListener: (Drug) -> Unit
    ) {
        drugTitle.text = drug.title
        remainingDrugCount.text = drug.drugCount.toString()
        dateStart.text = drug.alarmTime.toString()
        drugTitle.setOnClickListener { onTitleSelectedListener(drug) }
        containerView.setOnLongClickListener { onDrugClickedListener(drug); true; }

    }
}