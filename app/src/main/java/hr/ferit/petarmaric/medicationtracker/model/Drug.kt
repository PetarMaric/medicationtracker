package hr.ferit.petarmaric.medicationtracker.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.time.LocalTime

@Entity(tableName = "Drugs")
data class Drug(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val title: String,
    var drugCount: Int,
    val alarmTime: LocalTime
)