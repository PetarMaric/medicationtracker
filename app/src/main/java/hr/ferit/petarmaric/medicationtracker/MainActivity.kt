package hr.ferit.petarmaric.medicationtracker

import android.view.Menu
import android.view.MenuItem
import hr.ferit.petarmaric.medicationtracker.common.createAlertDialog
import hr.ferit.petarmaric.medicationtracker.common.displayToast
import hr.ferit.petarmaric.medicationtracker.common.showFragment
import hr.ferit.petarmaric.medicationtracker.ui.BaseActivity
import hr.ferit.petarmaric.medicationtracker.ui.druglist.DrugListFragment
import hr.ferit.petarmaric.medicationtracker.ui.listeners.BottomNavigationListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity() {

    override fun getLayoutResourceId(): Int = R.layout.activity_main

    override fun setUpUi() {

        showDrugList()

        bottomNavigationView.setOnNavigationItemSelectedListener(BottomNavigationListener(this))
    }

    private fun showDrugList() {
        showFragment(R.id.fragmentContainer, DrugListFragment())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.menu_options, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            R.id.deleteAllDrugs -> {
                deleteAllDrugsDialog()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun deleteAllDrugsDialog() {

        createAlertDialog(
            this,
            "Do you want to delete all drugs and their reminders?",
            "Delete all",
            "Cancel",
            { deleteAllDrugs() },
            { })
    }

    private fun deleteAllDrugs() {
        if (supportFragmentManager.findFragmentById(R.id.fragmentContainer) is DrugListFragment) {
            (supportFragmentManager.findFragmentById(R.id.fragmentContainer) as DrugListFragment).deleteAllDrugs()
        } else {
            displayToast("Can not delete drugs from this view.")
        }


    }


}
