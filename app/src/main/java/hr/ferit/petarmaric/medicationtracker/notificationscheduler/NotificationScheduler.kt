package hr.ferit.petarmaric.medicationtracker.notificationscheduler

import android.app.AlarmManager
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.ComponentName
import android.content.Context
import android.content.Context.ALARM_SERVICE
import android.content.Intent
import android.content.pm.PackageManager
import android.icu.util.Calendar
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import hr.ferit.petarmaric.medicationtracker.R
import hr.ferit.petarmaric.medicationtracker.common.CHANNEL_DRUG_REMINDER
import hr.ferit.petarmaric.medicationtracker.common.createNotificationChannels
import hr.ferit.petarmaric.medicationtracker.common.getChannelId
import hr.ferit.petarmaric.medicationtracker.model.Drug
import hr.ferit.petarmaric.medicationtracker.notificationscheduler.AlarmReceiver.Companion.DRUG_ID
import hr.ferit.petarmaric.medicationtracker.persistence.DrugsRepository
import hr.ferit.petarmaric.medicationtracker.persistence.DrugsRoomRepository

// https://developer.android.com/training/scheduling/alarms
// https://droidmentor.com/schedule-notifications-using-alarmmanager/

class NotificationScheduler {

    companion object {
        private const val TAG = "NotificationScheduler"
        private const val DAILY_REMINDER_REQUEST_CODE = 1337


        fun <T> setReminder(context: Context, receiverClass: Class<T>, drug: Drug) {

            val setCalendar = Calendar.getInstance()
            setCalendar.set(Calendar.HOUR_OF_DAY, drug.alarmTime.hour)
            setCalendar.set(Calendar.MINUTE, drug.alarmTime.minute)
            setCalendar.set(Calendar.SECOND, 0)

            val receiver = ComponentName(context, receiverClass)
            val packageManager = context.packageManager

            packageManager.setComponentEnabledSetting(
                receiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP
            )


            Intent(context, receiverClass).apply {


                this.putExtra(DRUG_ID, drug.id)


                val pendingIntent = PendingIntent.getBroadcast(
                    context,
                    DAILY_REMINDER_REQUEST_CODE + drug.id,
                    this,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )

                val alarmManager = context.getSystemService(ALARM_SERVICE) as AlarmManager

                /*
                alarmManager.setInexactRepeating(
                    AlarmManager.RTC_WAKEUP,
                    setCalendar.timeInMillis,
                    AlarmManager.INTERVAL_DAY,
                    pendingIntent
                )
                */
                /**
                 * zbog jednostavnosti prikaza notifikacija korišten je kod ispod; u zamišljenoj uporabi koristio bi se gornji zakomentirani dio koda
                 */
                alarmManager.setRepeating(
                    AlarmManager.RTC_WAKEUP,
                    setCalendar.timeInMillis,
                    1000 * 60,
                    pendingIntent
                )
            }

        }

        fun <T> cancelReminder(context: Context, receiverClass: Class<T>, drug: Drug) {

            Intent(context, receiverClass).apply {

                this.putExtra(DRUG_ID, drug.id)

                val pendingIntent = PendingIntent.getBroadcast(
                    context,
                    DAILY_REMINDER_REQUEST_CODE + drug.id,
                    this,
                    PendingIntent.FLAG_UPDATE_CURRENT
                )

                val alarmManager = context.getSystemService(ALARM_SERVICE) as AlarmManager

                alarmManager.cancel(pendingIntent)
                pendingIntent.cancel()

                Log.d(TAG, "One broadcast canceled")
            }
        }

        fun <T> showNotification(context: Context, receiverClass: Class<T>, drug: Drug) {

            val repository: DrugsRepository = DrugsRoomRepository()

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) createNotificationChannels()

            val notificationIntent = Intent(context, receiverClass)
            notificationIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP


            val stackBuilder = TaskStackBuilder.create(context)
            stackBuilder.addParentStack(receiverClass)
            stackBuilder.addNextIntent(notificationIntent)

            val pendingIntent =
                stackBuilder.getPendingIntent(DAILY_REMINDER_REQUEST_CODE + drug.id, PendingIntent.FLAG_UPDATE_CURRENT)

            val notificationBuilder = NotificationCompat.Builder(context, getChannelId(CHANNEL_DRUG_REMINDER))

            drug.drugCount -= 1

            val notification = notificationBuilder.setSmallIcon(R.drawable.ic_medicine)
                .setContentTitle("Please consume ${drug.title}")
                .setContentText("You have ${drug.drugCount} left. ${if (drug.drugCount < 7) "Please renew your ${drug.title} inventory." else "\b"}")
                .setAutoCancel(true)
                .setContentIntent(pendingIntent)
                .build()

            repository.updateDrug(drug)

            NotificationManagerCompat.from(context).notify(
                drug.id,
                notification
            )


        }

        fun <T> disableAlarmReceiver(context: Context, receiverClass: Class<T>) {
            val receiver = ComponentName(context, receiverClass)
            val packageManager = context.packageManager

            packageManager.setComponentEnabledSetting(
                receiver, PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP
            )

            Log.d(TAG, "DISABLED ALARM RECEIVER")
        }
    }


}