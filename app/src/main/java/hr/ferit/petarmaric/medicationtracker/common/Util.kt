package hr.ferit.petarmaric.medicationtracker.common

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import hr.ferit.petarmaric.medicationtracker.R
import hr.ferit.petarmaric.medicationtracker.RootAppActivity
import hr.ferit.petarmaric.medicationtracker.model.Drug

const val BROWSER_ERROR_TAG = "Error_Browser_Intent"

const val BASE_URL = "https://mediately.co/hr/drugs"

fun openDrugInfoInBrowser(drug: Drug) {

    val queryUrl = BASE_URL + "?q=${drug.title}"

    Intent().apply {
        data = Uri.parse(queryUrl)
        action = Intent.ACTION_VIEW
        flags = Intent.FLAG_ACTIVITY_NEW_TASK

        if (this.resolveActivity(RootAppActivity.getAppContext().packageManager) != null) {
            RootAppActivity.getAppContext().startActivity(this)
        } else {
            Log.d(
                BROWSER_ERROR_TAG,
                "Can not find any browser application. Please visit Play Store and install necessary application."
            )
        }
    }

}

fun createAlertDialog(
    context: Context,
    title: String,
    positiveButtonText: String,
    negativeButtonText: CharSequence,
    positiveAction: () -> Unit,
    negativeAction: () -> Unit
) {

    AlertDialog.Builder(context, R.style.AlertDialogDeleteDrugStyle)
        .setTitle(title)
        .setPositiveButton(positiveButtonText) { _, _ ->
            positiveAction()
        }
        .setNegativeButton(negativeButtonText) { _, _ -> negativeAction() }
        .create()
        .show()
}