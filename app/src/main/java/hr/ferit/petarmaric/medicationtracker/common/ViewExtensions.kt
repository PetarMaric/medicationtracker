package hr.ferit.petarmaric.medicationtracker.common

import android.view.View

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.hidden() {
    visibility = View.GONE
}