package hr.ferit.petarmaric.medicationtracker.persistence

import androidx.room.TypeConverter
import java.time.LocalTime
import java.time.format.DateTimeFormatter
import java.time.temporal.TemporalAccessor

class RoomTypeConverters {
    companion object {
        private val timeFormatter = DateTimeFormatter.ISO_LOCAL_TIME

        // https://docs.oracle.com/javase/8/docs/api/java/time/format/DateTimeFormatter.html
        // https://medium.com/androiddevelopers/room-time-2b4cf9672b98
        @TypeConverter
        @JvmStatic
        fun fromLocalTime(time: LocalTime?): String? {
            return time?.format(timeFormatter)
        }

        @TypeConverter
        @JvmStatic
        fun toLocalTime(time: String?): LocalTime? {
            return time?.let {
                val parsedTime: TemporalAccessor = timeFormatter.parse(it)
                return parsedTime.query(LocalTime::from)
            }
        }


    }
}