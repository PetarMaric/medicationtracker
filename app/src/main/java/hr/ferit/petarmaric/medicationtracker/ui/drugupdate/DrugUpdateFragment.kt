package hr.ferit.petarmaric.medicationtracker.ui.drugupdate

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import hr.ferit.petarmaric.medicationtracker.R
import hr.ferit.petarmaric.medicationtracker.common.displayToast
import hr.ferit.petarmaric.medicationtracker.model.Drug
import hr.ferit.petarmaric.medicationtracker.notificationscheduler.AlarmReceiver
import hr.ferit.petarmaric.medicationtracker.notificationscheduler.NotificationScheduler
import hr.ferit.petarmaric.medicationtracker.persistence.DrugsRepository
import hr.ferit.petarmaric.medicationtracker.persistence.DrugsRoomRepository
import hr.ferit.petarmaric.medicationtracker.ui.drugadd.TimePickerFragment
import kotlinx.android.synthetic.main.fragment_add_drug.*
import java.time.LocalTime

class DrugUpdateFragment : Fragment(), TimePickerFragment.TimeSetListener {

    private lateinit var timeToUpdate: LocalTime


    override fun timePicked(hourParam: Int, minuteParam: Int) {
        timeSelectedText.text = "$hourParam:$minuteParam"
        timeToUpdate = LocalTime.of(hourParam, minuteParam)
    }

    private var drugId = NO_DRUG

    private val repository: DrugsRepository = DrugsRoomRepository()
    private var storedDrugId: Int = -1

    private lateinit var storedDrug: Drug


    companion object {
        const val NO_DRUG = -1
        const val EXTRA_DRUG_ID = "extra_task_id"

        fun newInstance(drugId: Int): DrugUpdateFragment {
            val bundle = Bundle().apply { putInt(EXTRA_DRUG_ID, drugId) }
            return DrugUpdateFragment().apply { arguments = bundle }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // reuseanje fragmenta za dodavanje lijeka
        return inflater.inflate(R.layout.fragment_add_drug, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        arguments?.getInt(EXTRA_DRUG_ID)?.let { drugId = it }


        setUpUi()
        tryToDisplayDrug(drugId)


    }

    private fun setUpUi() {
        saveDrugButton.text = getString(R.string.updateDrugInfoButtonText)

        val timePickerDialog = TimePickerFragment()
        timePickerDialog.setTimePickedListener(this)

        showTimePickerDialog.setOnClickListener { _ -> timePickerDialog.show(childFragmentManager, "time_picker") }

        saveDrugButton.setOnClickListener { updateDrug() }
    }

    private fun updateDrug() {
        if (isInputInvalid()) {
            context?.displayToast(getString(R.string.checkFormText))
            return
        }

        val updatedDrug = Drug(
            id = drugId,
            title = drugName.text.toString(),
            drugCount = drugCount.text.toString().toInt(),
            alarmTime = timeToUpdate
        )

        repository.updateDrug(updatedDrug)

        NotificationScheduler.setReminder(context!!, AlarmReceiver::class.java, updatedDrug)

        context?.displayToast(getString(R.string.successfulDrugInfoUpdateText))
    }

    private fun isInputInvalid(): Boolean {
        if (drugName.text.isNullOrEmpty() || drugCount.text.isNullOrEmpty()) {
            return true
        }
        return false
    }

    private fun tryToDisplayDrug(drugId: Int) {
        try {
            val drug = repository.getDrug(drugId)
            storedDrugId = drug.id

            storedDrug = drug

            drug.alarmTime?.let { timeToUpdate = it }
            displayDrug(drug)
        } catch (e: NullPointerException) {
            context?.displayToast(getString(R.string.noDrugFoundText))
        } catch (e: NoSuchElementException) {
            context?.displayToast(getString(R.string.noDrugFoundText))
        }
    }

    private fun displayDrug(drug: Drug) {
        drugName.setText(drug.title)
        drugCount.setText(drug.drugCount.toString())
        timeSelectedText.text = drug.alarmTime.toString()

    }


}