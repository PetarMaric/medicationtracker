package hr.ferit.petarmaric.medicationtracker.common

import android.app.NotificationChannel
import android.app.NotificationManager
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationManagerCompat
import hr.ferit.petarmaric.medicationtracker.RootAppActivity

fun getChannelId(name: String): String = "${RootAppActivity.getAppContext().packageName}-$name"

const val CHANNEL_DRUG_REMINDER = "Drug_reminder_channel"

@RequiresApi(api = Build.VERSION_CODES.O)
fun createNotificationChannel(name: String, description: String, importance: Int): NotificationChannel {
    val channel = NotificationChannel(getChannelId(name), name, importance)
    channel.description = description
    channel.setShowBadge(true)
    return channel
}

@RequiresApi(api = Build.VERSION_CODES.O)
fun createNotificationChannels() {
    val channels = mutableListOf<NotificationChannel>()
    channels.add(
        createNotificationChannel(
            CHANNEL_DRUG_REMINDER,
            "Notification channel to show drug consumption info",
            NotificationManagerCompat.IMPORTANCE_DEFAULT
        )
    )

    val notificationManager = RootAppActivity.getAppContext().getSystemService(NotificationManager::class.java)
    notificationManager.createNotificationChannels(channels)
}
