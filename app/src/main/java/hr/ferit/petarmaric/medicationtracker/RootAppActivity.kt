package hr.ferit.petarmaric.medicationtracker

import android.app.Application
import android.content.Context

class RootAppActivity : Application() {
    companion object {
        private lateinit var instance: RootAppActivity

        fun getAppContext(): Context = instance.applicationContext
    }

    override fun onCreate() {
        instance = this
        super.onCreate()
    }
}