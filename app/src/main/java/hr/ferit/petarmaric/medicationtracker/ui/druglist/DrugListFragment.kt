package hr.ferit.petarmaric.medicationtracker.ui.druglist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.petarmaric.medicationtracker.R
import hr.ferit.petarmaric.medicationtracker.common.*
import hr.ferit.petarmaric.medicationtracker.model.Drug
import hr.ferit.petarmaric.medicationtracker.notificationscheduler.AlarmReceiver
import hr.ferit.petarmaric.medicationtracker.notificationscheduler.NotificationScheduler
import hr.ferit.petarmaric.medicationtracker.notificationscheduler.NotificationScheduler.Companion.disableAlarmReceiver
import hr.ferit.petarmaric.medicationtracker.persistence.DrugsRepository
import hr.ferit.petarmaric.medicationtracker.persistence.DrugsRoomRepository
import hr.ferit.petarmaric.medicationtracker.ui.adapter.DrugListAdapter
import hr.ferit.petarmaric.medicationtracker.ui.drugupdate.DrugUpdateFragment
import kotlinx.android.synthetic.main.fragment_drugs.*

class DrugListFragment : Fragment() {

    private val drugList = mutableListOf<Drug>()

    private val repository: DrugsRepository = DrugsRoomRepository()

    private val drugsAdapter by lazy {
        DrugListAdapter({ onDrugClicked(it) }, { onTitleClicked(it) })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_drugs, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        noStoredDrugs.visible()
        getAllDrugs()


        initializeDrugList()
        setDrugListSwipeAction()

    }

    private fun initializeDrugList() {
        savedDrugsRecyclerView.apply {
            adapter = drugsAdapter
            layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
            itemAnimator = DefaultItemAnimator()
        }
    }

    private fun setDrugListSwipeAction() {
        val itemSwipeCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {

                val position = viewHolder.adapterPosition

                val drug = drugList[position]

                showDeleteConfirmationDialog(drug)
            }

        }

        val itemTouchHelper = ItemTouchHelper(itemSwipeCallback)
        itemTouchHelper.attachToRecyclerView(savedDrugsRecyclerView)
    }

    private fun showDeleteConfirmationDialog(drug: Drug) {

        createAlertDialog(
            context!!,
            "Do you want to remove '${drug.title}' from local database?",
            "Delete",
            "Cancel",
            { handlePositiveButtonAction(drug) },
            { handleNegativeButtonAction() })
    }

    private fun handlePositiveButtonAction(drug: Drug) {
        NotificationScheduler.cancelReminder(
            context!!, AlarmReceiver::class.java, drug
        )

        activity?.displayToast("Drug ${drug.title} successfully removed along with its alarm.")

        repository.deleteDrug(drug)
        drugList.remove(drug)
        getAllDrugs()

    }

    private fun handleNegativeButtonAction() {
        drugsAdapter.notifyDataSetChanged()
    }


    private fun getAllDrugs() {
        drugList.clear()
        drugList.addAll(repository.getAllDrugs())

        if (drugList.isNotEmpty()) {
            noStoredDrugs.hidden()
        } else {
            noStoredDrugs.visible()
            disableAlarmReceiver(context!!, AlarmReceiver::class.java)
        }

        drugsAdapter.setData(drugList)

    }


    private fun onTitleClicked(drug: Drug) {
        openDrugInfoInBrowser(drug)
    }

    private fun onDrugClicked(drug: Drug) {
        activity?.showFragment(
            R.id.fragmentContainer,
            DrugUpdateFragment.newInstance(drugId = drug.id)
        )
    }

    fun deleteAllDrugs() {

        drugList.forEach {
            NotificationScheduler.cancelReminder(
                context!!, AlarmReceiver::class.java, it
            )
        }

        repository.deleteAllDrugs()
        noStoredDrugs.visible()
        drugList.clear()

        drugsAdapter.setData(drugList)

    }


}