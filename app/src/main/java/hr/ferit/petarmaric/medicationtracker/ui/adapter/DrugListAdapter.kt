package hr.ferit.petarmaric.medicationtracker.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.petarmaric.medicationtracker.R
import hr.ferit.petarmaric.medicationtracker.model.Drug
import hr.ferit.petarmaric.medicationtracker.ui.druglist.DrugViewHolder

class DrugListAdapter(
    private val onDrugClickedListener: (Drug) -> Unit,
    private val onTitleClickedListener: (Drug) -> Unit
) : RecyclerView.Adapter<DrugViewHolder>() {

    private val drugList = mutableListOf<Drug>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrugViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_drug, parent, false)
        return DrugViewHolder(view)
    }

    override fun getItemCount(): Int = drugList.size

    override fun onBindViewHolder(holder: DrugViewHolder, position: Int) {
        val drug = drugList.get(position)
        holder.bind(drug, onDrugClickedListener, onTitleClickedListener)
    }

    fun setData(drugDataList: List<Drug>) {
        drugList.clear()
        drugList.addAll(drugDataList)
        notifyDataSetChanged()
    }


}